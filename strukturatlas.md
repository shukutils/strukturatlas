# Strukturatlas des Mietshäusersyndikats
Welche Gruppen gibt es und wie sind sie erreichbar?

## AG Chillen

Diese AG hat noch keine Beschreibung von sich veröffentlicht.
Gehörst du zu dieser AG? Dann solltest du in dem SyndiDAT-Gruppenordner deiner AG diese Datei finden. Verändere ihren
Inhalt und wir aktualisieren innerhalb von 24 Stunden diesen Eintrag automatisch.



## AG IT

Wir treffen uns alle zwei Wochen und halten die Bits und Bytes für euch am Laufen.
01101000100101

###Contact

Ihr erreicht uns unter ag-it@somethingsomething
Selbst organisieren tun wir uns über Github.



## AG Struktur

###AG Strukture


Wir machen euch eine schöne Struktur hier. Joa.

Nett, oder?



## Beratung Hamburg

Moinsen. Beratung Hamburg am Start.



## Beratung Nimmerland

Hallöchen!

Wir sind von der Beratung Nimmerland. Ihr erreicht uns unter nimmer@land.de, aber wir sind hauptsächlich aktiv über
unsere Signal-Gruppe: fragt einfach Julia aus dem Projekt Tinkerbell.



## Region Freiburg

Servus, wir in Freiburg sind sehr organisiert und haben sogar eine Beschreibung hier.
Erreichen tut man uns am Besten vor Ort im Strandcafé, notfalls geht auch eine Mail an mail@freiburg.xyz
