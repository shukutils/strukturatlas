import re
from os import listdir
from os.path import isfile, join
import pypandoc

path = '/home/yunohost.app/nextcloud/data/shuki/files/strukturtest'
result_file_name = 'strukturatlas.md'

header = """# Strukturatlas des Mietshäusersyndikats
Welche Gruppen gibt es und wie sind sie erreichbar?

"""

onlyfiles = sorted([f for f in listdir(path) if isfile(join(path, f)) and f.endswith('.md') and not f in ['README.md',
    result_file_name]])

markdown_contents = []

for filename in onlyfiles:
    with open(join(path, filename), 'r') as f:
        content = f.read()
    # If there is a top-level headline (with one # or two ##), demote all headlines by one for consistency
    while (re.search(r"^\#\s.*", content) or re.search(r"^\#\#\s.*", content) or
           re.search(r"\n\#\s.*", content) or re.search(r"\n\#\#\s.*", content)):
        content = re.sub(r"\#\s", '##', content)

    content = f"## {filename[:-3]}\n\n" + content
    markdown_contents.append(content)

joined_markdown = header + '\n\n\n'.join(markdown_contents)

with open(join(path, result_file_name), 'w') as f:
    f.write(joined_markdown)

pypandoc.convert_file(result_file_name, 'latex', outputfile=result_file_name[:-3]+'.pdf')

